import { createSlice } from '@reduxjs/toolkit'

const authSlice = createSlice({
  name: 'auth',
  initialState: { token: null },
  reducers: {
    setCredentials: (state, action) => {
      const { accessToken } = action.payload
      /*
        since we are inside authSlice where state name 'auth' is located
        we do not need to do state.auth.token
      */
      state.token = accessToken
    },
    logOut: (state, action) => {
      state.token = null
    },
  },
})

export const { setCredentials, logOut } = authSlice.actions

export default authSlice.reducer

/*
    since we are outside authSlice where state name 'auth' is located
    we need to do state.auth.token
*/
export const selectCurrentToken = (state) => state.auth.token
